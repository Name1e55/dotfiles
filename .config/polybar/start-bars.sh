#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Start different bars on main and aux displays
if type "xrandr"; then
    mons=($(xrandr --query | grep " connected" | cut -d" " -f1))
    value=($(xrandr --query | grep " connected" | cut -d" " -f3))
    for (( i=0 ; i < ${#mons[@]}; i++ )); do
        if [ "${value[i]}" = "primary" ]; then 
            MONITOR="${mons[i]}" polybar --reload main &
        else
            MONITOR=${mons[i]} polybar --reload aux &
        fi
  done
# If there's only one monitor, start fill featured one
else
  polybar --reload combined &
fi

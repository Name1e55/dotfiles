#!/bin/sh
# Profile file. Runs on login.

# Adds `~/.scripts` and all subdirectories to $PATH
export PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export EDITOR="nvim"
export TERMINAL="alacritty"
export TERM="alacritty"
export TERMCMD="alacritty"
export terminal="alacritty"
export BROWSER="firefox"
export READER="zathura"
export FILE="vifm"
export QT_QPA_PLATFORMTHEME="gtk2"
# USE ripgrep for FZF
export FZF_DEFAULT_COMMAND='rg --files --hidden --smart-case -g "!{.vscode-oss,.wine,.omnisharp,.git,sources,.cache}"'

#Better colors for NTFS
eval "$(dircolors ~/.dir_colors)"
[ ! -f ~/.config/shortcutrc ] && shortcuts >/dev/null 2>&1



#!/bin/bash

# change gaps with increment of 5. No param resets to 5
gap=$(bspc query --tree --desktop focused | jq '.windowGap')
case $1 in
  "up")
    gap=$((gap+5))
    ;;
  "down")
    [[ $gap -ge 5 ]] && gap=$((gap-5)) || gap=0
    ;;
  *)
    gap=5
    ;;
esac
bspc config window_gap $gap

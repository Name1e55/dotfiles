#!/bin/bash

#notify-send "$1";
class=$(xprop -id "$1" WM_CLASS | cut -d\" -f4 | uniq);
name=$(xprop -id "$1" WM_NAME | cut -d\" -f2 | uniq);
#notify-send "$class";
case $class in
  "firefox")
      echo "desktop=1"
      ;;
  "Vmware-view")
      echo "desktop=4"
      ;;
  "Skype")
      echo "desktop=6"
      echo "state=tiled"
      echo "focus=off"
      ;;
  "Microsoft Teams - Preview")
      echo "desktop=6"
      echo "state=tiled"
      echo "focus=off"
      ;;
  "TelegramDesktop")
      case $t in
        "Media viewer")
            echo "desktop=6"
            echo "state=floating"
            echo "focus=off"
            ;;
        "Telegram")
            echo "desktop=6"
            echo "state=tiled"
            echo "focus=off"
            ;;
      esac
      ;;
esac

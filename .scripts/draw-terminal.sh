#!/bin/bash

# read coordinates via slop
read -r X Y W H < <(slop -f "%x %y %w %h" -b 2 -l -t 0 -q)

# get dimensions in font characters
(( W/= 10 ))
(( H/= 20 ))

# add one-shot rule and spawn terminal
bspc rule -a Alacritty -o state=floating follow=on focus=on
alacritty --position $X $Y --dimensions $W $H

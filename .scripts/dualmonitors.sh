#! /usr/bin/env zsh


#Here's check for monitor's existance
#set up the two monitors for bspwm
# NOTE This is a simplistic approach because I already know the settings I
# want to apply.
#dual_setup=$(xrandr --query | grep 'HDMI-0')
#if [[ $dual_setup = *connected* ]]; then
#  xrandr --output DP-4 --primary --mode 2560x1440 --rate 144.00 --pos 1920x0 --rotate normal --output HDMI-0 --mode 1920x1080 --pos 0x360  --rotate normal
#fi

# Get list of connected monitors from XRandR, sort by name so DP-x is the first one
#temp=$(xrandr --query | grep -i " connected " | cut -d " " -f1 | sort)
monitors=($(xrandr --query | grep -i " connected " | cut -d " " -f1 | sort))

if [[ ${#monitors[@]} == 2 ]]; then
    xrandr --dpi 108 --output $monitors[1] --primary --mode 2560x1440 --rate 144.00 --pos 1920x0 --rotate normal --output $monitors[2] --mode 1920x1080 --pos 0x360  --rotate normal
fi

# Use this also to setup bspwm mapping
echo $monitors

#!/bin/bash

#do nothing if param not supplied
[[ -z $1 ]] && exit

desk=$(bspc query -D -d focused)
case $1 in
  "prev")
    bspc desktop -m prev
    bspc monitor -f prev
  ;;
  "next")
    bspc desktop -m next
    bspc monitor -f next
  ;;
  *)
  ;;
esac
bspc desktop -f $desk
reorder-desktops.sh

#!/bin/bash

# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute
# requires pulseaudio and pulsemixer

function is_mute {
    pulsemixer --get-mute
}

function send_notification {
    volume=$(pulsemixer --get-volume | cut -f 1 -d " ")
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
    bar=$(seq -s "─" $(($volume / 5)) | sed 's/[0-9]//g')
    
    #icon="~/faba-icon-theme/Faba/48x48/notifications/notification-audio-volume-medium.svg"
    # Send the notification
    dunstify -r 2593 -u normal "Volume: $bar"
}

case $1 in
    up)
        # Set the volume on (if it was muted)
        # amixer -D pulse set Master on > /dev/null
        # Up the volume (+ 2%)
        pulsemixer --change-volume +2
        #send_notification
        ;;
    down)
        #amixer -D pulse set Master on > /dev/null
        #amixer -D pulse sset Master 5%- > /dev/null
        pulsemixer --change-volume -2
        #send_notification
        ;;
    mute)
        # Toggle mute
        amixer -D pulse set Master 1+ toggle > /dev/null
        if is_mute ; then
            dunstify -i audio-volume-muted -t 8 -r 2593 -u normal "Mute"
        else
            send_notification
        fi
        ;;
esac

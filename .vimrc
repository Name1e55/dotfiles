"0 Plugins
"-------------------------------------------------------------------------------
    " For plug-ins to load correctly.
    filetype plugin indent on
    call plug#begin('~/.vim/plugged')
        Plug 'tpope/vim-surround'
        "Plug 'flazz/vim-colorschemes'
        Plug 'tpope/vim-commentary'
        Plug 'itchyny/lightline.vim'
        Plug 'mengelbrecht/lightline-bufferline'
        Plug 'ludovicchabant/vim-gutentags'
        Plug 'scrooloose/nerdtree'
        Plug 'tpope/vim-commentary'
        Plug 'bling/vim-bufferline'
        Plug 'unblevable/quick-scope'
        Plug 'kovetskiy/sxhkd-vim'
        Plug 'morhetz/gruvbox'
        Plug 'arcticicestudio/nord-vim'
        Plug 'norcalli/nvim-colorizer.lua'
        Plug 'liuchengxu/vim-which-key'
    call plug#end()

"1 OS-specific things
"-------------------------------------------------------------------------------
    if ($OS == 'Windows_NT')
        " Windows specific settings
        " 1.1 Restore cursor to file position in previous editing session http://vim.wikia.com/wiki/VimTip80
        set viminfo='10,\"100,:20,%,n$VIM/_viminfo
        " 1.2 executing OS command within Vim
        set shell=c:\Windows\system32\cmd.exe
        " shell command flag
        set undodir=C:\Windows\Temp
        set shellcmdflag=/c
        "For Win GUI set window size. Just in case.
        win 150 200 " The window height
    else
        "Unix specific settings
        " 1.1 : pick it from $HOME/.viminfo
        set viminfo='10,\"100,:20,%,n~/.viminfo
        "redraw bg 'cos vim sucks with kitty
        let &t_ut=''
        set undodir=~/.vim/undodir
        " 1.2 executing OS command within Vim
        set shell=/bin/zsh
    endif

"2 Initial configuration
"-------------------------------------------------------------------------------
    set nocompatible
    set shortmess+=I

    set encoding=utf-8
    set fileencodings=utf-8,cp1251,ucs-2le

    " add ls to the end of the file
    set fileformat=unix

    " make C-6 switch layout in insert mode
    set keymap=russian-jcukenwin
    set iminsert=0
    set imsearch=-1
    inoremap <ESC> <ESC>:set iminsert=0<CR>

    " Do not keep a backup or .swp file.
    set nobackup nowritebackup noswapfile

    "Persistent undo
    set undofile

    " read/write a .viminfo file, don't store more than 50 lines of registers
    set viminfo='20,\"50

    "Tab settings
    set tabstop=4 shiftwidth=4 expandtab

    set autoindent smartindent
    set splitbelow splitright
    set mousehide
    set hidden
    set path=.,,**

    set tags=tags;/
    set tagstack

    :set mps+=<:>
    " Only first line is checked for  modelines
    set modelines=1
    " Speed up scrolling a bit
    set ttyfast

    " allow backspacing over everything in insert mode
    " set backspace=2

    " Search
    set incsearch hlsearch ignorecase smartcase

    " show matching brackets
    set showmatch

    " Switch on syntax highlighting
    syntax on

    " Display 5 lines above/below the cursor when scrolling with a mouse.
    set scrolloff=5

    set clipboard=unnamedplus

    "Set the history size to maximum. by default it is 20 - http://vim.wikia.com/wiki/VimTip45
    set history=80

    set autoread wildmode=longest,list,full
    "set  wildmenu 
    setlocal foldmethod=indent
    "set foldlevel=5

"3 Colors, look-and-feel
"-------------------------------------------------------------------------------

    set formatoptions=tcqrn1
    set guifont="JetBrains Mono":h12

    "Add some colors for lightline plugin
    let g:lightline = {
        \ 'colorscheme': 'nord',
        \ 'active': {
        \     'left': [ [ 'mode', 'paste' ],
        \             [ 'readonly', 'absolutepath', 'modified' ] ]
        \ },
        \ 'inactive': {
        \     'left': [ [ 'absolutepath' ] ]
        \ },
        \ }
    let g:lightline.tabline = {
        \ 'left': [ ['tabs'] ],
        \ 'right': [ ['close'] ]
        \}

    "Apply VIM colorscheme
    colorscheme nord
    set background=dark termguicolors cursorline
    set number relativenumber title

    " wraps longs lines to screen size
    set wrap

    " Don't break words when wrapping
    set linebreak

    " Show ↪ at the beginning of wrapped lines
    if has("linebreak")
        let &sbr = nr2char(8618).' '
    endif

    " Highlight matching pairs of brackets. Use the '%' character to jump between them.
    set matchpairs+=<:>

    " Display invisible characters
    set list
    set listchars=tab:▸\ ,trail:·,extends:❯,precedes:❮,nbsp:×

    set noshowmode "Mode is now in statusline
    set vb t_vb= " stop beeping or flashing the screen
    set noerrorbells
    set novisualbell
    set tm=500

    set laststatus=2 " Show the status line even if only one file is being edited
    "set ruler " Show ruler
    " Make command line one line high
    set ch=1

"4 Mappings
"-------------------------------------------------------------------------------
    let g:mapleader=" "
    let maplocalleader=","

    noremap Q q
    " Disable <Arrow keys>
    " Warning: nightmare mode!
    "inoremap <up> <NOP>
    "inoremap <down> <NOP>
    "inoremap <left> <NOP>
    "inoremap <right> <NOP>
    noremap <up> <NOP>
    noremap <down> <NOP>
    noremap <left> <NOP>
    noremap <right> <NOP>

    " Navigate with <Ctrl>-hjkl in Insert mode
    inoremap <C-h> <C-o>h
    inoremap <C-j> <C-o>j
    inoremap <C-k> <C-o>k
    inoremap <C-l> <C-o>l

    "<Esc><Esc>
    " Clear the search highlight in Normal mode
    nnoremap <silent> <Esc><Esc> :nohlsearch<CR><Esc>

    " < >
    vnoremap < <gv
    vnoremap > >gv

    " Move lines
    xnoremap <C-S-j> :move '<-2<CR>gv-gv
    xnoremap <C-S-k> :move '>+1<CR>gv-gv

    " Y from cursor position to the end of line
    nnoremap Y y$

    " Switch splits
    nnoremap <C-h> <C-W>h
    nnoremap <C-j> <C-W>j
    nnoremap <C-k> <C-W>k
    nnoremap <C-l> <C-W>l

    " Close split
    "nnoremap <C-q> <C-w>q

    " n и N
    " Search matches are always in center
    nnoremap n nzz
    nnoremap N Nzz
    nnoremap * *zz
    nnoremap # #zz
    nnoremap g* g*zz
    nnoremap g# g#zz

    " Navigate through wrapped lines
    noremap j gj
    noremap k gk

    " gf
    " Open file under cursor in a new vertical split
    nnoremap gf :<C-u>vertical wincmd f<CR>

    " В коммандном режиме разрешить прыгать в конец и начало строки,
    " как в консоли
    cnoremap <c-e> <end>
    inoremap <c-e> <c-o>$
    cnoremap <c-a> <home>
    inoremap <c-a> <c-o>^

    " Bind :Q to :q
    command! Q q

    " {<CR>
    " auto complete {} indent and position the cursor in the middle line
    inoremap {<CR> {<CR>}<Esc>O
    inoremap (<CR> (<CR>)<Esc>O
    inoremap [<CR> [<CR>]<Esc>O

    " Switch tabs with <Tab>
    nnoremap <Tab> gt
    nnoremap <S-Tab> gT


    "<leader> mappings

    " ,m
        " Toggle mouse support in Normal mode
        set mouse=a
        function! ToggleMouse()
          if &mouse == 'a'
            set mouse=
            echo "Mouse usage disabled"
          else
            set mouse=a
            echo "Mouse usage enabled"
          endif
        endfunction
        nnoremap <leader>m :call ToggleMouse()<CR>

    " ,s
        " Shortcut for :%s//
        nnoremap <leader>s :<C-u>%s//<left>
        vnoremap <leader>s :s//<left>

    " ,p
        " Toggle the 'paste' option
        set pastetoggle=<Leader>p

    " ,v
        " Open the .vimrc in a new tab
        nnoremap <leader>v :<C-u>tabedit $MYVIMRC<CR>

    " ,td
        " Open todo.txt in a new tab
        nnoremap <leader>td :<C-u>tabedit i:\Org_Stuff\TODO.txt<CR>


    " Create a new window relative to the current one
        nnoremap <Leader><left>  :<C-u>leftabove  vnew<CR>
        nnoremap <Leader><right> :<C-u>rightbelow vnew<CR>
        nnoremap <Leader><up>    :<C-u>leftabove  new<CR>
        nnoremap <Leader><down>  :<C-u>rightbelow new<CR>

    " Copy indented line without spaces
        nnoremap <Leader>y ^yg_"_dd

    " Fold with space
        nnoremap <Leader><Space> za
        vnoremap <Leader><Space> zf

    " ,ts
        " Fix trailing whitespace
        nnoremap <leader>ts :<C-u>%s/\s\+$//e<CR>

    " ,bl
        " Show buffers
        nnoremap <Leader>bl :<C-u>ls<cr>:b

    " ,bp
        " Go to prev buffer
        nnoremap <Leader>bp :<C-u>bp<cr>

    " ,bn
        " Go to next buffer
        nnoremap <Leader>bn :<C-u>bn<cr>

    " ,u
        " Change case to uppercase
        nnoremap <Leader>u gUiw
        inoremap <Leader>u <esc>gUiwea

    " ,w
        " Jump to next split
        nnoremap <Leader>w <C-w>w

    " ,n
        " Edit another file in the same directory with the current one
        noremap <Leader>n :<C-u>vnew <C-R>=expand("%:p:h") . '/'<CR>

    " ,fu
    "Find file with word under cursor. Some kind of Find usages
        map <leader>fu :noau execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

        map <leader>fw :noau execute "vimgrep /" . expand("") . "/j **" <Bar> cw<S-Left><S-Left>
    " Remap like Find implementation
        map <leader>fi <C-]>


    " session save\restore
    map <F2> :mksession! ~/vim_session <CR>
    map <F3> :source ~/vim_session <CR>

    "Copy current filename with path to clipboard
    map     <F8> :let @* = expand('%:p')<cr>
    map!    <F8> <Esc>:let @* = expand('%:p')<cr>


"5 Menus
"-------------------------------------------------------------------------------
    " Create encodings menu
    menu Encoding.UTF-8 :e ++enc=utf8 <CR>
    menu Encoding.Windows-1251 :e ++enc=cp1251<CR>
    menu Encoding.koi8-r :e ++enc=koi8-r<CR>
    menu Encoding.cp866 :e ++enc=cp866<CR>
    menu Encoding.ucs-2le :e ++enc=ucs-2le<CR>

    " Spell checking
    if version >= 700
        " Turn off spell checking
        set nospell
        menu Spell.off :setlocal spell spelllang= <cr>
        menu Spell.Russian+English :setlocal spell spelllang=ru,en <cr>
        menu Spell.Russian :setlocal spell spelllang=ru <cr>
        menu Spell.English :setlocal spell spelllang=en <cr>
        menu Spell.-SpellControl- :
        menu Spell.Word\ Suggest<Tab>z= z=
        menu Spell.Previous\ Wrong\ Word<Tab>[s [s
        menu Spell.Next\ Wrong\ Word<Tab>]s ]s
    endif

"6 Autocommands
"-------------------------------------------------------------------------------
    "Auto change the directory to the current file I'm working on
    autocmd BufEnter * lcd %:p:h

    " Resize splits when the window is resized
    au VimResized * exe "normal! \<c-w>="

"7 Plugin related settings
"-------------------------------------------------------------------------------
    " NERDTree
        nnoremap <Bs> :<C-u>NERDTreeToggle<CR>
        let NERDTreeShowBookmarks=1
        let NERDTreeChDirMode=2
        let NERDTreeQuitOnOpen=1
        let NERDTreeShowHidden=1
        let NERDTreeKeepTreeInNewTab=0
        " Disable display of the 'Bookmarks' label and 'Press ? for help' text
        let NERDTreeMinimalUI=0
        " Use arrows instead of + ~ chars when displaying directories
        let NERDTreeDirArrows=1
        let NERDTreeBookmarksFile= $HOME . '/.vim/.NERDTreeBookmarks'

    "TODO.txt will try to highlight overdue with Python
    let g:todo_load_python = 1
"----------------------------------------------------------------------------------------------------------
